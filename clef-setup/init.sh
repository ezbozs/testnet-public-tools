#! /usr/bin/zsh

alias dcrun="docker-compose -f docker-compose-init.yaml run --rm"

if [[ "$@" == *"node"* ]] || [[ "$@" == "" ]]; then
	echo initing node
	dcrun node-init
fi

if [[ "$@" == *"master"* ]] || [[ "$@" == "" ]]; then
	echo initing masterseed 
	dcrun --entrypoint "clef init" signer-init
fi
if [[ "$@" == *"rule"* ]] || [[ "$@" == "" ]]; then
	echo updating rules
	dcrun --entrypoint "clef attest `sha256sum clef/rules.js`" signer-init 
fi
 
